<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Funcao;

class FuncaoController extends Controller
{
    private $funcao;

    public function __construct(Funcao $funcao)
    {
        $this->funcao = $funcao;
    }
    
    public function index()
    {
        $funcoes = $this->funcao->all();

        return view('painel.funcoes.index', compact('funcoes'));
    }

    public function permissoes($id)
    {   
        //recupera a funcao
        $funcao = $this->funcao->find($id);

        //recuperar permissoes
        $permissoes = $funcao->permissoes;

        return view('painel.funcoes.permissoes', compact('funcao', 'permissoes'));
    }
}
