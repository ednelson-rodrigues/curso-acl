<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Funcao;
use App\Models\Permissao;
use App\Models\Post;

class PainelController extends Controller
{
    public function index()
    {
        $totalUsers = User::count();
        $totalFuncoes = Funcao::count();
        $totalPermissoes = Permissao::count();
        $totalPosts = Post::count();

        return view('painel.home.index', compact('totalUsers','totalFuncoes','totalPermissoes','totalPosts'));
    }
}
