<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permissao;

class PermissaoController extends Controller
{
    private $permissao;

    public function __construct(Permissao $permissao)
    {
        $this->permissao = $permissao;
    }
    
    public function index()
    {
        $permissoes = $this->permissao->all();

        return view('painel.permissoes.index', compact('permissoes'));
    }
}
