<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Models\Post;
use App\User;
use Gate;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        return view('portal.home.index');
    }

    public function update($idPost)
    {
        $post = Post::find($idPost);

        //$this->authorize('update-post', $post);
        if (Gate::denies('update-post', $post))
            abort(403, 'Não autorizado');

        return view('update-post', compact('post'));
    }

    public function funcoesPermissoes()
    {
        $userName = auth()->user()->name;
        echo "<h1>$userName</h1>";

        //funcoes atribuidas a esse usuario
        echo "<strong>FUNÇÕES E SUAS PERMISSÕES</strong><br>";
        foreach (auth()->user()->funcoes as $funcao) {
            echo "<strong>$funcao->nome</strong> = ";

            $permissoes = $funcao->permissoes;
            foreach ($permissoes as $permissao) {
                echo " $permissao->nome, ";
            }

            echo "<hr>";
        }
    }
}
