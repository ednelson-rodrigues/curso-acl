<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funcao extends Model
{
    protected $table = 'funcoes';

    //retorna todas as permissoes da funcao
    public function permissoes()
    {
        return $this->belongsToMany(Permissao::class);
    }
}
