<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permissao extends Model
{
    protected $table = 'permissoes';

    //recupera todas as funcoes dessa permissao
    public function funcoes()
    {
        return $this->belongsToMany(Funcao::class);
    }
}
