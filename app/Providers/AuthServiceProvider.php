<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Post;
use App\User;
use App\Models\Permissao;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Models\Post' => 'App\Policies\PostPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /*
        Gate::define('update-post', function(User $user, Post $post){
            return $user->id == $post->user_id;
        });
        */

        $permissoes = Permissao::with('funcoes')->get();
        foreach ($permissoes as $permissao) {
            Gate::define($permissao->nome, function(User $user) use ($permissao){
                return $user->hasPermission($permissao);
            });
        }

        //metodo para super admin, vai sempre verificar antes de qualquer metodo
        Gate::before(function(User $user, $ability){
            
            if ($user->hasAnyFuncoes('adm'))
                return true;

        });

    }
}
