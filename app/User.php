<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Permissao;
use App\Models\Funcao;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //retorna as funcoes dos usuarios
    public function funcoes()
    {
        return $this->belongsToMany(Funcao::class);
    }

    public function hasPermission(Permissao $permissao)
    {
        return $this->hasAnyFuncoes($permissao->funcoes);
    }

    public function hasAnyFuncoes($funcoes)
    {
        // verificar se as funcoes estao ou nao associadas ao usuario
        if (is_array($funcoes) || is_object($funcoes)) {
            return !! $funcoes->intersect($this->funcoes)->count();
        }

        //Gerente
        return $this->funcoes->contains('nome', $funcoes);
    }
}
