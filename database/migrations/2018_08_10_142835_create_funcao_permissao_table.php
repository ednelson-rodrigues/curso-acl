<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncaoPermissaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcao_permissao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('funcao_id')->unsigned();
            $table->integer('permissao_id')->unsigned();

            $table->foreign('funcao_id')
                        ->references('id')
                        ->on('funcoes')
                        ->onDelete('cascade');
            $table->foreign('permissao_id')
                        ->references('id')
                        ->on('permissoes')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcao_permissao');
    }
}
