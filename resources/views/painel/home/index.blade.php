@extends('painel.templates.template')

@section('content')

<div class="relatorios">
		<div class="container">
			<ul class="relatorios">
				<li class="col-md-6 text-center">
					<a href="/painel/posts">
						<img src="{{url('assets/painel/imgs/noticias-acl.png')}}" alt="Posts" class="img-menu">
						<h1>{{$totalPosts}}</h1>
					</a>
				</li>
				<li class="col-md-6 text-center">
					<a href="/painel/funcoes">
						<img src="{{url('assets/painel/imgs/funcao-acl.png')}}" alt="Funcoes" class="img-menu">
						<h1>{{$totalFuncoes}}</h1>
					</a>
				</li>
				<li class="col-md-6 text-center">
					<a href="/painel/permissoes">
						<img src="{{url('assets/painel/imgs/permissao-acl.png')}}" alt="Permissoes" class="img-menu">
						<h1>{{$totalPermissoes}}</h1>
					</a>
				</li>
				<li class="col-md-6 text-center">
					<a href="/painel/users">
						<img src="{{url('assets/painel/imgs/perfil-acl.png')}}" alt="Usuarios" class="img-menu">
						<h1>{{$totalUsers}}</h1>
					</a>
				</li>
			</ul>
		</div>
	</div><!--Relatorios-->

@endsection