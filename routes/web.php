<?php

$this->group(['prefix' => 'painel'], function (){
    //PainelController
    $this->get('/', 'Painel\PainelController@index')->name('painel');

    //PostController
    $this->get('posts', 'Painel\PostController@index');

    //PermissaoController
    $this->get('permissoes', 'Painel\PermissaoController@index');

    //FuncaoController
    $this->get('funcoes', 'Painel\FuncaoController@index');
    $this->get('funcao/{id}/permissoes', 'Painel\FuncaoController@permissoes');

    //UserController
    $this->get('users', 'Painel\UserController@index');
});

Auth::routes();

Route::get('/', 'Portal\SiteController@index');
